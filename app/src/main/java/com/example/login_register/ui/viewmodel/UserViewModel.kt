package com.example.login_register.ui.viewmodel

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.login_register.data.model.User
import com.example.login_register.data.repository.UserRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class UserViewModel : ViewModel() {

    private val userRepository = UserRepository()
    private val viewModelScope = CoroutineScope(Dispatchers.IO)

    private val _userLiveData = MutableLiveData<User?>()
    val userLiveData: LiveData<User?> get() = _userLiveData

    fun getUserFromPrefs(context: Context) {
        viewModelScope.launch {
            userRepository.getUserFromPrefs(context) { user ->
                _userLiveData.postValue(user)
            }
        }
    }

    fun saveUserToPrefs(context: Context, user: User) {
        viewModelScope.launch {
            userRepository.saveUserToPrefs(context, user) {
                _userLiveData.postValue(user)
            }
        }
    }
}
