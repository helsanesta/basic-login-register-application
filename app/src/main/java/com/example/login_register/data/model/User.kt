package com.example.login_register.data.model

data class User(
    val username: String,
    val password: String
)