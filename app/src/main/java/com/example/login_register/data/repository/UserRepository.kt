package com.example.login_register.data.repository

import android.content.Context
import com.example.login_register.data.model.User
import com.google.gson.Gson

class UserRepository {

    companion object {
        private const val PREFS_NAME = "MyPrefs"
        private const val USER_KEY = "user_key"
    }

    fun getUserFromPrefs(context: Context, callback: (User?) -> Unit) {
        val sharedPreferences = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE)
        val userJson = sharedPreferences.getString(USER_KEY, null)
        val user = if (userJson != null) {
            Gson().fromJson(userJson, User::class.java)
        } else {
            null
        }
        callback(user)
    }

    fun saveUserToPrefs(context: Context, user: User, callback: () -> Unit) {
        val sharedPreferences = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE)
        val editor = sharedPreferences.edit()
        val userJson = Gson().toJson(user)
        editor.putString(USER_KEY, userJson)
        editor.apply()
        callback()
    }
}
