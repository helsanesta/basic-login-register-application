package com.example.login_register.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.login_register.R

class WelcomePageActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_welcome_page)
    }
}